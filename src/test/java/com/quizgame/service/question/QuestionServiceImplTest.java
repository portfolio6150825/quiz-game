package com.quizgame.service.question;

import com.quizgame.dto.category.CategoryDTO;
import com.quizgame.dto.question.QuestionDTO;
import com.quizgame.entity.question.QuestionEntity;
import com.quizgame.repository.question.QuestionRepository;
import com.quizgame.service.category.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;

public class QuestionServiceImplTest {

    private QuestionService questionService;

    private QuestionRepository questionRepository = Mockito.mock(QuestionRepository.class);

    private CategoryService categoryService = Mockito.mock(CategoryService.class);


    @Before
    public void init() {
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setQuestionId(1L);
        questionEntity.setQuestionText("Testowe Pytanie");
        questionEntity.setAnswers(new ArrayList<>());

        Mockito.when(questionRepository.findById(Mockito.anyLong())).thenReturn(questionEntity);

        Mockito.when(categoryService.getCategory(Mockito.any())).thenReturn(new CategoryDTO(1L, "TestCategory", "/test_image.jpg"));
        questionService = new QuestionServiceImpl(questionRepository, categoryService, new ModelMapper());
    }


    @Test
    public void findById_shouldReturnQuestionDTOIfProvidedCorrectId() {
        QuestionDTO questionDTO = questionService.findById(1L);
        Assertions.assertEquals("Testowe Pytanie", questionDTO.getQuestionText());
    }

}