package com.quizgame.service.question;

import com.quizgame.dto.question.QuestionDTO;

import java.util.List;

public interface QuestionService {

    QuestionDTO save(QuestionDTO questionDTO);

    List<QuestionDTO> findAll();

    QuestionDTO findById(Long id);

    void deleteById(Long theId);

    List<QuestionDTO> findByCategoryName(String name);

}
