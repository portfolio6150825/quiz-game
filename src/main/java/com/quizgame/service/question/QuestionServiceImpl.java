package com.quizgame.service.question;

import com.quizgame.dto.category.CategoryDTO;
import com.quizgame.dto.question.QuestionDTO;
import com.quizgame.entity.category.CategoryEntity;
import com.quizgame.entity.question.QuestionEntity;
import com.quizgame.repository.question.QuestionRepository;
import com.quizgame.service.category.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final CategoryService categoryService;
    private final ModelMapper modelMapper;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository, CategoryService categoryService, ModelMapper modelMapper) {
        this.questionRepository = questionRepository;
        this.categoryService = categoryService;
        this.modelMapper = modelMapper;
    }

    @Override
    public QuestionDTO save(QuestionDTO questionDTO) {
        CategoryDTO category = categoryService.getCategory(questionDTO.getCategory().getName());
        QuestionEntity questionEntity = modelMapper.map(questionDTO, QuestionEntity.class);
        questionEntity.setCategory(modelMapper.map(category, CategoryEntity.class));
        QuestionEntity save = questionRepository.save(questionEntity);
        return modelMapper.map(save, QuestionDTO.class);
    }

    @Override
    public List<QuestionDTO> findAll() {
        return questionRepository.findAll()
                .stream()
                .map(questionEntity -> modelMapper.map(questionEntity, QuestionDTO.class))
                .toList();
    }

    @Override
    public QuestionDTO findById(Long id) {
        if (id == null) return null;
        QuestionEntity entity = questionRepository.findById(id);







        return modelMapper.map(entity, QuestionDTO.class);
    }

    @Override
    public void deleteById(Long theId) {
        questionRepository.deleteById(theId);
    }

    @Override
    public List<QuestionDTO> findByCategoryName(String name) {
        return questionRepository.findByCategoryName(name)
                .stream()
                .map(questionEntity -> modelMapper.map(questionEntity, QuestionDTO.class))
                .toList();
    }
}

