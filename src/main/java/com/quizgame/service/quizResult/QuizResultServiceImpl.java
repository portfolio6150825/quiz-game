package com.quizgame.service.quizResult;

import com.quizgame.dto.category.CategoryDTO;
import com.quizgame.dto.quizResult.QuizResultDTO;
import com.quizgame.entity.category.CategoryEntity;
import com.quizgame.entity.quizResult.QuizResultEntity;
import com.quizgame.entity.user.UserEntity;
import com.quizgame.repository.quiz.QuizResultRepository;
import com.quizgame.repository.user.UserRepository;
import com.quizgame.service.category.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuizResultServiceImpl implements QuizResultService {

    private final QuizResultRepository quizResultRepository;
    private final ModelMapper modelMapper;
    private final CategoryService categoryService;
    private final UserRepository userRepository;

    public QuizResultServiceImpl(QuizResultRepository quizResultRepository, ModelMapper modelMapper, CategoryService categoryService, UserRepository userRepository) {
        this.quizResultRepository = quizResultRepository;
        this.modelMapper = modelMapper;
        this.categoryService = categoryService;
        this.userRepository = userRepository;
    }


    @Override
    public List<QuizResultDTO> findAllByUsername(String username) {
        return quizResultRepository.findAllByUser_Login(username, Sort.by(Sort.Direction.DESC, "finalScore"))
                .stream()
                .map(quizResultEntity -> modelMapper.map(quizResultEntity, QuizResultDTO.class))
                .toList();
    }

    @Override
    public List<QuizResultDTO> findAllQuizResults() {
        return quizResultRepository.findAll(Sort.by(Sort.Direction.DESC, "finalScore"))
                .stream()
                .map(quizResultEntity -> modelMapper.map(quizResultEntity, QuizResultDTO.class))
                .toList();

    }

    @Override
    public QuizResultDTO saveQuizResult(QuizResultDTO quizResultDTO) {
        CategoryDTO category = categoryService.getCategory(quizResultDTO.getCategoryName());
        QuizResultEntity quizResultEntity = modelMapper.map(quizResultDTO, QuizResultEntity.class);
        quizResultEntity.setCategoryEntity(modelMapper.map(category, CategoryEntity.class));
        Optional<UserEntity> userByLogin = userRepository.findUserByLogin(quizResultDTO.getUser().getLogin());
        UserEntity userEntity = userByLogin.orElseThrow(() -> new RuntimeException("User doesn't exist: " + quizResultDTO.getUser().getLogin()));
        quizResultEntity.setUser(userEntity);
        return modelMapper.map(quizResultRepository.save(quizResultEntity), QuizResultDTO.class);
    }
}
