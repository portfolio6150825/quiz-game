package com.quizgame.service.quizResult;

import com.quizgame.dto.quizResult.QuizResultDTO;

import java.util.List;

public interface QuizResultService {

    List<QuizResultDTO> findAllByUsername(String username);

    List<QuizResultDTO> findAllQuizResults();

    QuizResultDTO saveQuizResult(QuizResultDTO quizResultDTO);
}
