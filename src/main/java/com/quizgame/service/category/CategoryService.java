package com.quizgame.service.category;

import com.quizgame.dto.category.CategoryDTO;

import java.util.List;

public interface CategoryService {

    List<CategoryDTO> findAll();

    CategoryDTO getCategory(String categoryName);


}
