package com.quizgame.service.category;

import com.quizgame.dto.category.CategoryDTO;
import com.quizgame.entity.category.CategoryEntity;
import com.quizgame.repository.category.CategoryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, ModelMapper modelMapper) {
        this.categoryRepository = categoryRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<CategoryDTO> findAll() {
        return categoryRepository.findAll()
                .stream()
                .map(categoryEntity -> modelMapper.map(categoryEntity, CategoryDTO.class))
                .toList();
    }

    @Override
    public CategoryDTO getCategory(String categoryName) {
        CategoryEntity categoryByName = categoryRepository.getCategoryByName(categoryName);
        return modelMapper.map(categoryByName, CategoryDTO.class);
    }
}
