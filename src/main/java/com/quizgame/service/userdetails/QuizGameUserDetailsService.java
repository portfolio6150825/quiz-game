package com.quizgame.service.userdetails;

import com.quizgame.entity.user.UserEntity;
import com.quizgame.repository.user.UserPrincipal;
import com.quizgame.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class QuizGameUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public QuizGameUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<UserEntity> userByLogin = userRepository.findUserByLogin(login);
        if (userByLogin.isPresent()) {
            return new UserPrincipal(userByLogin.get());
        } else {
            throw new UsernameNotFoundException(String.format("User: %s is not found", login));
        }
    }
}
