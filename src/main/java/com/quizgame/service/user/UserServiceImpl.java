package com.quizgame.service.user;

import com.quizgame.dto.registration.RegistrationUserDTO;
import com.quizgame.dto.user.UserDTO;
import com.quizgame.entity.user.Role;
import com.quizgame.entity.user.RoleType;
import com.quizgame.entity.user.UserEntity;
import com.quizgame.repository.user.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    @Override
    public UserDTO saveUserRegistration(RegistrationUserDTO registrationUserDTO) {
        UserEntity entity = modelMapper.map(registrationUserDTO, UserEntity.class);
        Role role = new Role();
        role.setName(RoleType.ROLE_USER);

        entity.setRoles(Collections.singleton(role));
        entity.setPassword(passwordEncoder.encode(registrationUserDTO.getPassword()));
        UserEntity saveUser = userRepository.save(entity);

        return modelMapper.map(saveUser, UserDTO.class);
    }

    @Override
    public UserDTO findUserByLogin(String login) throws UsernameNotFoundException {
        Optional<UserEntity> userByLogin = userRepository.findUserByLogin(login);
        if (userByLogin.isPresent()) {
            return modelMapper.map(userByLogin.get(), UserDTO.class);
        } else {
            throw new UsernameNotFoundException(String.format("User: %s is not found", login));
        }
    }
}
