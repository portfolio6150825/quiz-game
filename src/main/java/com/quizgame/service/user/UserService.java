package com.quizgame.service.user;

import com.quizgame.dto.registration.RegistrationUserDTO;
import com.quizgame.dto.user.UserDTO;

public interface UserService {
    UserDTO saveUserRegistration(RegistrationUserDTO registrationUserDTO);

    UserDTO findUserByLogin(String login);

}
