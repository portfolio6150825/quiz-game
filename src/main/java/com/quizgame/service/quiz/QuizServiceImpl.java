package com.quizgame.service.quiz;

import com.quizgame.dto.answer.AnswerDTO;
import com.quizgame.dto.answer.UserAnswerDTO;
import com.quizgame.dto.answer.UserAnswerSummary;
import com.quizgame.dto.question.QuestionDTO;
import com.quizgame.dto.quizResult.QuizResultDTO;
import com.quizgame.dto.user.UserDTO;
import com.quizgame.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class QuizServiceImpl implements QuizService {
    private final UserService userService;

    @Autowired
    public QuizServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public QuizResultDTO validateAnswers(List<UserAnswerDTO> userAnswerList) {
        int finalScore = 0;
        List<UserAnswerSummary> userAnswerSummaries = new ArrayList<>();
        for (UserAnswerDTO userAnswerDTO : userAnswerList) {
            String userAnswer = userAnswerDTO.getAnswer();
            QuestionDTO questionDTO = userAnswerDTO.getQuestionDTO();
            for (AnswerDTO answerDTO : questionDTO.getAnswers()) {
                if (answerDTO.getAnswerText().equals(userAnswer)) {
                    userAnswerSummaries.add(new UserAnswerSummary(questionDTO, userAnswer, answerDTO.isCorrect(), questionDTO.getCorrectAnswer()));
                    if (answerDTO.isCorrect()) {
                        finalScore = finalScore + 1;
                    }
                }
            }
        }
        String login = userAnswerList.get(0).getUsername();
        UserDTO userDTO = userService.findUserByLogin(login);

        return QuizResultDTO.builder()
                .user(userDTO)
                .finalScore(finalScore)
                .percentageResult((finalScore * 100) / userAnswerList.size())
                .createdAt(LocalDateTime.now())
                .answeredQuestionsNumber(userAnswerList.size())
                .userAnswerSummaries(userAnswerSummaries)
                .categoryName(userAnswerList.get(0).getQuestionDTO().getCategory().getName())
                .build();
    }
}
