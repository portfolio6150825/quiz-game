package com.quizgame.service.quiz;

import com.quizgame.dto.quizResult.QuizResultDTO;
import com.quizgame.dto.answer.UserAnswerDTO;

import java.util.List;

public interface QuizService {
    QuizResultDTO validateAnswers(List<UserAnswerDTO> userAnswerList);
}
