package com.quizgame.dto.quizResult;

import com.quizgame.dto.answer.UserAnswerSummary;
import com.quizgame.dto.user.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuizResultDTO {

    private UserDTO user;
    private List<UserAnswerSummary> userAnswerSummaries;
    private int finalScore;
    private int answeredQuestionsNumber;
    private int percentageResult;
    private String categoryName;
    private LocalDateTime createdAt;
}
