package com.quizgame.dto.answer;

import com.quizgame.dto.question.QuestionDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserAnswerSummary {
    private QuestionDTO questionDTO;
    private String userAnswer;
    private boolean isCorrect;
    private AnswerDTO correctAnswer;
}
