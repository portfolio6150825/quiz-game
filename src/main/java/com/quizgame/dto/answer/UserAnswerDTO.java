package com.quizgame.dto.answer;

import com.quizgame.dto.question.QuestionDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserAnswerDTO {

    private String username;
    private Integer questionIdx;
    private QuestionDTO questionDTO;
    private String answer;

}
