package com.quizgame.dto.answer;

import com.quizgame.dto.question.QuestionDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AnswerDTO {

    private Long answerId;

    private String answerText;

    private boolean isCorrect;

    private QuestionDTO question;

    @Override
    public String toString() {
        return "AnswerDTO{" +
                "answerId=" + answerId +
                ", answerText='" + answerText + '\'' +
                ", isCorrect=" + isCorrect +
                '}';
    }
}
