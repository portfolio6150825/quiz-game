package com.quizgame.dto.question;

import com.quizgame.dto.answer.AnswerDTO;
import com.quizgame.dto.category.CategoryDTO;
import com.quizgame.entity.question.QuestionsDifficulty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionDTO {

    private Long questionId;
    private String questionText;
    private CategoryDTO category;
    private List<AnswerDTO> answers;
    private QuestionsDifficulty questionsDifficulty;

    public AnswerDTO getCorrectAnswer() {
        for (AnswerDTO answerDTO : answers) {
            if (answerDTO.isCorrect()) {
                return answerDTO;
            }
        }
        throw new IllegalStateException("No correct answer in the question: " + questionText);
    }
}