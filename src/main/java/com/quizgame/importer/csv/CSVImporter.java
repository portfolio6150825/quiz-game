package com.quizgame.importer.csv;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import com.quizgame.dto.answer.AnswerDTO;
import com.quizgame.dto.category.CategoryDTO;
import com.quizgame.dto.question.QuestionDTO;
import com.quizgame.entity.question.QuestionsDifficulty;
import com.quizgame.service.question.QuestionService;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class CSVImporter implements FileImporter {

    private final QuestionService questionService;

    public CSVImporter(QuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    public void importQuestionsFromCSV(MultipartFile file) {
        try (CSVReader csvReader = new CSVReader(new InputStreamReader(file.getInputStream()))) {
            String[] headers = csvReader.readNext();
            String[] line;
            while ((line = csvReader.readNext()) != null) {
                int isCorrect = Integer.parseInt(line[5]);

                QuestionDTO questionDTO = new QuestionDTO();
                List<AnswerDTO> answerDTO = new ArrayList<>();

                answerDTO.add(new AnswerDTO(null, line[1], 0 == isCorrect, questionDTO));
                answerDTO.add(new AnswerDTO(null, line[2], 1 == isCorrect, questionDTO));
                answerDTO.add(new AnswerDTO(null, line[3], 2 == isCorrect, questionDTO));
                answerDTO.add(new AnswerDTO(null, line[4], 3 == isCorrect, questionDTO));

                questionDTO.setQuestionText(line[0]);
                questionDTO.setAnswers(answerDTO);
                CategoryDTO categoryDTO = new CategoryDTO();
                categoryDTO.setName(line[6]);
                questionDTO.setCategory(categoryDTO);
                questionDTO.setQuestionsDifficulty(QuestionsDifficulty.valueOf(line[7]));

                questionService.save(questionDTO);
            }
        } catch (CsvValidationException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}