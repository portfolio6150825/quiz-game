package com.quizgame.importer.csv;

import org.springframework.web.multipart.MultipartFile;

public interface FileImporter {

    void importQuestionsFromCSV(MultipartFile file);

}
