package com.quizgame.entity.question;

public enum QuestionsDifficulty {
    EASY,
    MODERATE,
    DIFFICULT,
    CHALLENGING,
    ADVANCED
}
