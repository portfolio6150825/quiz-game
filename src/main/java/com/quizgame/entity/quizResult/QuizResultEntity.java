package com.quizgame.entity.quizResult;

import com.quizgame.entity.category.CategoryEntity;
import com.quizgame.entity.user.UserEntity;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "quiz_results")
public class QuizResultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "categoryEntity_id")
    private CategoryEntity categoryEntity;

    @ManyToOne
    @JoinColumn(name = "userEntity_id")
    private UserEntity user;

    @Column(name = "final_score")
    private int finalScore;

    @Column(name = "percentage_result")
    private int percentageResult;

    @CreatedDate
    @Column(name = "created_at")
    private LocalDateTime createdAt;

}
