package com.quizgame.entity.user;

public enum RoleType {

    ROLE_USER,
    ROLE_ADMIN
}
