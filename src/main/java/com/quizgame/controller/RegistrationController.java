package com.quizgame.controller;

import com.quizgame.dto.registration.RegistrationUserDTO;
import com.quizgame.dto.user.UserDTO;
import com.quizgame.service.user.UserService;
import jakarta.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegistrationController {

    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        model.addAttribute("user", new UserDTO());
        return "registration";
    }

    @PostMapping("/register")
    public String processRegistrationForm(@ModelAttribute("user") RegistrationUserDTO registrationUserDTO) {
        try {
            userService.saveUserRegistration(registrationUserDTO);
        } catch (DataIntegrityViolationException e) {
            return "registration-duplicate-exception";
        } catch (ConstraintViolationException e){
            return "registration-email-exception";
        }
        return "redirect:/";
    }
}
