package com.quizgame.controller;

import com.quizgame.dto.quizResult.QuizResultDTO;
import com.quizgame.service.quizResult.QuizResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class LeaderboardController {

    private final QuizResultService quizResultService;

    @Autowired
    public LeaderboardController(QuizResultService quizResultService) {
        this.quizResultService = quizResultService;
    }

    @GetMapping("/leaderboard")
    public String showLeaderboard(Model theModel) {
        List<QuizResultDTO> allQuizResults = quizResultService.findAllQuizResults();
        theModel.addAttribute("myQuizList", allQuizResults);
        return "/leaderboard";
    }
}
