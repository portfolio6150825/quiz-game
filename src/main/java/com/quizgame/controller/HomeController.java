package com.quizgame.controller;


import com.quizgame.dto.category.CategoryDTO;
import com.quizgame.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class HomeController {

    private final CategoryService categoryService;

    @Autowired
    public HomeController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/")
    public String showMainPage(Model model, Principal principal) {
        List<CategoryDTO> categoryDTOList = categoryService.findAll();
        model.addAttribute("categories", categoryDTOList);
        if (principal != null) {
            model.addAttribute("username", principal.getName());
        }
        return "main";
    }
}
