package com.quizgame.controller;

import com.quizgame.importer.csv.CSVImporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileImportController {
    private final CSVImporter csvImporter;

    @Autowired
    public FileImportController(CSVImporter csvImporter) {
        this.csvImporter = csvImporter;
    }

    @PostMapping("/importQuestions")
    public String uploadQuestions(@RequestParam("file") MultipartFile file) {
        csvImporter.importQuestionsFromCSV(file);
        return "redirect:/questions/list";
    }
}


