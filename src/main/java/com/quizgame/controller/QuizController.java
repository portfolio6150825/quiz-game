package com.quizgame.controller;

import com.quizgame.dto.question.QuestionDTO;
import com.quizgame.dto.quizResult.QuizResultDTO;
import com.quizgame.dto.answer.UserAnswerDTO;
import com.quizgame.service.question.QuestionService;
import com.quizgame.service.quiz.QuizService;
import com.quizgame.service.quizResult.QuizResultService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@SessionAttributes("userAnswers")
public class QuizController {

    private final QuestionService questionService;
    private final QuizService quizService;
    private final QuizResultService quizResultService;

    public QuizController(QuestionService questionService, QuizService quizService, QuizResultService quizResultService) {
        this.questionService = questionService;
        this.quizService = quizService;
        this.quizResultService = quizResultService;
    }

    @GetMapping("/quiz/{category}/{questionIdx}")
    public String showQuestion(@PathVariable(value = "category") String categoryName, @PathVariable(value = "questionIdx") int questionIdx, Model model) {
        List<QuestionDTO> questionList = questionService.findByCategoryName(categoryName);
        if (questionIdx >= 1 && questionIdx <= 12) {
            model.addAttribute("question", questionList.get(questionIdx - 1));
            return "quiz";
        } else {
            return String.format("redirect:/quiz?categoryName=%s&index=1", categoryName);
        }
    }

    @GetMapping("/quiz/{category}/{questionIdx}/submit")
    public String submitAnswer(Model model, Principal principal, @PathVariable(value = "questionIdx") int questionNumber,
                               @PathVariable(value = "category") String category, @RequestParam String selectedAnswer,
                               @RequestParam Long questionId, HttpSession session) {
        List<UserAnswerDTO> userAnswerDTOS = (List<UserAnswerDTO>) session.getAttribute("userAnswers");
        if (userAnswerDTOS == null) {
            userAnswerDTOS = new ArrayList<>();
        }
        if (!isAnswerAlreadySubmitted(userAnswerDTOS, questionNumber)) {
            QuestionDTO questionDTO = questionService.findById(questionId);
            userAnswerDTOS.add(new UserAnswerDTO(principal.getName(), questionNumber, questionDTO, selectedAnswer));
            session.setAttribute("userAnswers", userAnswerDTOS);

            if (questionNumber < 12) {
                return "redirect:/quiz/" + category + "/" + (questionNumber + 1);
            } else {
                QuizResultDTO quizResultDTO = quizService.validateAnswers(userAnswerDTOS);
                model.addAttribute("quizResult", quizResultDTO);
                session.removeAttribute("userAnswers");
                quizResultService.saveQuizResult(quizResultDTO);
                return "result";
            }
        }
        return "redirect:/quiz/" + category + "/" + (questionNumber + 1);
    }

    private boolean isAnswerAlreadySubmitted(List<UserAnswerDTO> userAnswerDTOS, int questionNumber) {
        return userAnswerDTOS.stream()
                .map(UserAnswerDTO::getQuestionIdx)
                .anyMatch(idx -> idx == questionNumber);
    }
}
