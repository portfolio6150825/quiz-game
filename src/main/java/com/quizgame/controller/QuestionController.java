package com.quizgame.controller;

import com.quizgame.dto.answer.AnswerDTO;
import com.quizgame.dto.question.QuestionDTO;
import com.quizgame.service.question.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/questions")
public class QuestionController {
    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/add")
    public String showAddQuestionForm(Model theModel) {
        theModel.addAttribute("question", new QuestionDTO());
        return "questions-and-answers-add-form";
    }

    @GetMapping("/list")
    public String getQuestionList(Model theModel) {
        List<QuestionDTO> questionDTOList = questionService.findAll();
        theModel.addAttribute("questionList", questionDTOList);
        return "list-of-questions";
    }

    @PostMapping("/add")
    public String upsertQuestion(@ModelAttribute("question") QuestionDTO questionDTO) {
        QuestionDTO existingQuestionDTO = questionService.findById(questionDTO.getQuestionId());

        if (existingQuestionDTO == null) existingQuestionDTO = questionDTO;

        existingQuestionDTO.setQuestionText(questionDTO.getQuestionText());

        List<AnswerDTO> answers = existingQuestionDTO.getAnswers();
        List<AnswerDTO> updatedAnswerEntities = questionDTO.getAnswers();

        for (int i = 0; i < answers.size(); i++) {
            AnswerDTO existingAnswerDTO = answers.get(i);
            AnswerDTO updatedAnswerDTO = updatedAnswerEntities.get(i);
            existingAnswerDTO.setAnswerText(updatedAnswerDTO.getAnswerText());
        }

        questionService.save(existingQuestionDTO);
        return "redirect:/questions/list";
    }

    @GetMapping("/modify")
    public String showUpdateQuestionForm(@RequestParam("question_id") Long id, Model model) {
        QuestionDTO questionDTO = questionService.findById(id);
        model.addAttribute("question", questionDTO);
        return "questions-and-answers-update-form";

    }

    @GetMapping("/delete")
    public String deleteQuestion(@RequestParam("question_id") Long id) {
        questionService.deleteById(id);
        return "redirect:/questions/list";
    }
}
