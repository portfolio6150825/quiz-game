package com.quizgame.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller

public class AboutUsController {

    @GetMapping("/about")
    private String aboutUs() {
        return "about";
    }
}
