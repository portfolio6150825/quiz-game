package com.quizgame.repository.quiz;

import com.quizgame.entity.quizResult.QuizResultEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizResultRepository extends JpaRepository<QuizResultEntity, Long> {

    List<QuizResultEntity> findAllByUser_Login(String login, Sort sort);

}
