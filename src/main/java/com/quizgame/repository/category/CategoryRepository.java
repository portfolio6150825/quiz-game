package com.quizgame.repository.category;

import com.quizgame.entity.category.CategoryEntity;

import java.util.List;

public interface CategoryRepository {

    List<CategoryEntity> findAll();

    CategoryEntity getCategoryByName(String categoryName);
}
