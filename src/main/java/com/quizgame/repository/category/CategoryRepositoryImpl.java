package com.quizgame.repository.category;

import com.quizgame.entity.category.CategoryEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private final EntityManager entityManager;

    @Autowired
    public CategoryRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<CategoryEntity> findAll() {
        TypedQuery<CategoryEntity> theQuery = entityManager.createQuery("from categories", CategoryEntity.class);
        return theQuery.getResultList();
    }

    @Override
    public CategoryEntity getCategoryByName(String categoryName) {
        TypedQuery<CategoryEntity> query = entityManager.createQuery(
                "from categories u  where u.name = :categoryName", CategoryEntity.class);
        query.setParameter("categoryName", categoryName);

        return query.getSingleResult();
    }
}