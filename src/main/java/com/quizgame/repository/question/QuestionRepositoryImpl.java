package com.quizgame.repository.question;

import com.quizgame.entity.answer.AnswerEntity;
import com.quizgame.entity.question.QuestionEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class QuestionRepositoryImpl implements QuestionRepository {

    private final EntityManager entityManager;

    @Autowired
    public QuestionRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public QuestionEntity save(QuestionEntity questionEntity) {
        for (AnswerEntity answerEntity : questionEntity.getAnswers()) {
            answerEntity.setQuestionEntity(questionEntity);
        }
        return entityManager.merge(questionEntity);
    }

    @Override
    public List<QuestionEntity> findAll() {
        TypedQuery<QuestionEntity> theQuery = entityManager.createQuery("from questions", QuestionEntity.class);
        return theQuery.getResultList();

    }

    @Override
    public QuestionEntity findById(Long id) {
        return entityManager.find(QuestionEntity.class, id);
    }

    @Override
    public void deleteById(Long theId) {
        QuestionEntity questionEntityToRemove = findById(theId);
        entityManager.remove(questionEntityToRemove);
    }

    @Override
    public List<QuestionEntity> findByCategoryName(String name) {
        Query query = entityManager.createQuery("FROM questions q WHERE q.category.name = :name", QuestionEntity.class);
        query.setParameter("name", name);
        List<QuestionEntity> questions = (List<QuestionEntity>) query.getResultList();
        return questions;
    }

    @Override
    public List<QuestionEntity> saveAll(List<QuestionEntity> questions) {
        for (QuestionEntity question : questions) {
            entityManager.persist(question);
        }
        return questions;
    }
}

