package com.quizgame.repository.question;

import com.quizgame.entity.question.QuestionEntity;

import java.util.List;

public interface QuestionRepository {

    QuestionEntity save(QuestionEntity questionEntity);

    List<QuestionEntity> findAll();

    QuestionEntity findById(Long id);

    void deleteById(Long theId);

    List<QuestionEntity> findByCategoryName(String name);

    List<QuestionEntity> saveAll(List<QuestionEntity> questions);
}
