package com.quizgame.repository.user;

import com.quizgame.dto.registration.RegistrationUserDTO;
import com.quizgame.entity.user.UserEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Query;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Log4j2
public class UserRepositoryImpl implements UserRepository {

    private final EntityManager entityManager;

    @Autowired
    public UserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Optional<UserEntity> findUserByEmail(String email) {
        try {
            Query query = entityManager.createQuery("FROM users u WHERE u.email = :email", UserEntity.class);
            query.setParameter("email", email);
            UserEntity userEntity = (UserEntity) query.getSingleResult();
            return Optional.ofNullable(userEntity);
        } catch (NoResultException noResultException) {
            log.warn("Email {} has not been found on database", email);
            return Optional.empty();
        }
    }

    @Override
    public UserEntity save(UserEntity userEntity) {
        return entityManager.merge(userEntity);
    }

    @Override
    public RegistrationUserDTO registrationSave(RegistrationUserDTO registrationUserDTO) {
        return entityManager.merge(registrationUserDTO);
    }

    @Override
    public Optional<UserEntity> findUserByLogin(String login) {
        try {
            Query query = entityManager.createQuery("FROM users u WHERE u.login = :login", UserEntity.class);
            query.setParameter("login", login);
            UserEntity singleResult = (UserEntity) query.getSingleResult();
            return Optional.ofNullable(singleResult);
        } catch (NoResultException noResultException) {
            log.warn("Login {} has not been found on database", login);
        }
        return Optional.empty();
    }
}
