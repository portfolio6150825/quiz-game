package com.quizgame.repository.user;

import com.quizgame.dto.registration.RegistrationUserDTO;
import com.quizgame.entity.user.UserEntity;

import java.util.Optional;

public interface UserRepository {

    Optional<UserEntity> findUserByEmail(String email);

    UserEntity save(UserEntity userEntity);

    RegistrationUserDTO registrationSave(RegistrationUserDTO registrationUserDTO);


    Optional<UserEntity> findUserByLogin(String login);

}
